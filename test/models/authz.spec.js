import {Authorization, AzRole} from '../../src/js/models/authz'

import {expect} from 'chai'

describe('AzRole', () => {
  it('should return true when checking deep equality with the same values', (done) => {
    const az1 = AzRole('a', 1)
    const az2 = AzRole('a', 1)
    expect(az1).to.deep.equal(az2)
    done()
  })

  it('should return false when checking deep equality for different values', (done) => {
    const az1 = AzRole('a', 1)
    const az2 = AzRole('b', 2)
    expect(az1).to.not.deep.equal(az2)
    done()
  })
})

describe('Authorization', () => {
  it('should return true when checking deep equality with the same values', (done) => {
    const az1 = Authorization(1, [AzRole('a', 1)])
    const az2 = Authorization(1, [AzRole('a', 1)])
    expect(az1).to.deep.equal(az2)
    done()
  })

  it('should return false when checking deep equality for different values', (done) => {
    const az1 = Authorization(1, [AzRole('a', 1)])
    const az2 = Authorization(2, [AzRole('b', 2)])
    expect(az1).to.not.deep.equal(az2)
    done()
  })

  it('should throw an exception when the memberId is not provided to the constructor', (done) => {
    expect(() => Authorization()).to.throw()
    done()
  })

  describe('.hasRole', () => {
    it('should return true when the given role name matches and the committeeId is null', (done) => {
      const az = Authorization(1, [AzRole('a'), AzRole('b')])
      expect(az.hasRole('b')).to.equal(true)
      done()
    })

    it("should return false when the given role name doesn't match and the committeeId is null", (done) => {
      const az = Authorization(1, [AzRole('a'), AzRole('b')])
      expect(az.hasRole('c')).to.equal(false)
      done()
    })

    it('should throw an exception when the second argument is not an object', (done) => {
      const az = Authorization(1, [])
      expect(() => az.hasRole('c', 1)).to.throw()
      done()
    })

    it("should return false when the given committeeId doesn't match the expected null value", (done) => {
      const az = Authorization(1, [AzRole('a'), AzRole('b')])
      expect(az.hasRole('b', {committeeId: 1})).to.equal(false)
      done()
    })

    it('should return true when the given role name and committeeId match', (done) => {
      const az = Authorization(1, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
      expect(az.hasRole('b', {committeeId: 1})).to.equal(true)
      done()
    })

    it("should return false when the given role name doesn't match but the committeeId does", (done) => {
      const az = Authorization(1, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
      expect(az.hasRole('c', {committeeId: 1})).to.equal(false)
      done()
    })

    it("should return false when the given committeeId doesn't match but the role name does", (done) => {
      const az = Authorization(1, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
      expect(az.hasRole('b', {committeeId: 3})).to.equal(false)
      done()
    })

    it('should return true when the memberId matches, regardless of roles', (done) => {
      const az = Authorization(1, [])
      expect(az.hasRole('anything', {memberId: 1})).to.equal(true)
      done()
    })

    it("should fallback to normal authorization rules when the memberId doesn't match and return true", (done) => {
      const az = Authorization(2, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
      expect(az.hasRole('b', {committeeId: 2, memberId: 1})).to.equal(true)
      done()
    })

    it("should fallback to normal authorization rules when the memberId doesn't match and return false", (done) => {
      const az = Authorization(2, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
      expect(az.hasRole('b', {committeeId: 3, memberId: 1})).to.equal(false)
      done()
    })
  })
})
