import { expect } from 'chai'
import { Map } from 'immutable'
import { IMPORT_MEMBERS } from '../../../src/js/redux/constants/actionTypes'
import member, {
  INITIAL_STATE
} from '../../../src/js/redux/reducers/memberImportReducers'

describe('memberImportReducers', () => {
  describe('SUBMIT', () => {
    it('updates the state', () => {
      const action = {
        type: IMPORT_MEMBERS.SUBMIT
      }

      const result = member(INITIAL_STATE, action).toJS()
      expect(result).to.deep.equal({ state: 'initiated' })
    })
  })

  describe('SUCCESS', () => {
    it('updates the state', () => {
      const action = {
        type: IMPORT_MEMBERS.SUCCESS,
        payload: Map({ total: 100 })
      }

      const result = member(INITIAL_STATE, action).toJS()
      expect(result).to.deep.equal({
        state: 'success',
        results: { total: 100 }
      })
    })
  })

  describe('FAILED', () => {
    it('updates the state', () => {
      const action = {
        type: IMPORT_MEMBERS.FAILED
      }

      const result = member(INITIAL_STATE, action).toJS()
      expect(result).to.deep.equal({ state: 'failure' })
    })
  })
})
