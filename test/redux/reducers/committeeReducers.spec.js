import { expect } from "chai";
import { fromJS } from "immutable";
import committees from "../../../src/js/redux/reducers/committeeReducers";
import { COMMITTEES } from "../../../src/js/redux/constants/actionTypes";

describe("committeeReducers", () => {
  describe("COMMITTEES.UPDATE_LIST", () => {
    it("adds a new committee to the committee list", () => {
      const state = fromJS({
        byId: {
          1: {
            id: 1,
            name: "test-1"
          }
        },
        other: "value"
      });
      const action = {
        type: COMMITTEES.UPDATE_LIST,
        payload: fromJS([
          {
            id: 2,
            name: "test-2"
          }
        ])
      };
      const result = committees(state, action).toJS();

      expect(result).to.deep.equal({
        byId: {
          1: {
            id: 1,
            name: "test-1"
          },
          2: {
            id: 2,
            name: "test-2"
          }
        },
        other: "value"
      });
    });

    it("updates an existing committee in the committee list", () => {
      const state = fromJS({
        byId: {
          1: {
            id: 1,
            name: "test-1"
          },
          2: {
            id: 2,
            name: "test-2"
          }
        },
        other: "value"
      });
      const action = {
        type: COMMITTEES.UPDATE_LIST,
        payload: fromJS([
          {
            id: 2,
            name: "updated-2"
          }
        ])
      };
      const result = committees(state, action).toJS();

      expect(result).to.deep.equal({
        byId: {
          1: {
            id: 1,
            name: "test-1"
          },
          2: {
            id: 2,
            name: "updated-2"
          }
        },
        other: "value"
      });
    });
  });
});
