import { expect } from 'chai'
import { fromJS } from 'immutable'
import {
  MEMBERS_CREATED,
  MEMBERS_FETCHED
} from '../../../src/js/redux/constants/actionTypes'
import members from '../../../src/js/redux/reducers/members'

describe('membersReducers', () => {
  function generateMemberData(memberId) {
    return {
      id: memberId,
      do_not_call: false,
      do_not_email: false,
      is_eligible: false,
      info: {
        email_address: `testmember${memberId}@dsausa.org`,
        first_name: `Test Member #${memberId}`,
        last_name: 'Last Name'
      },
      meetings: [
        'January 2000 General Meeting',
        'January 2000 Special Meeting'
      ],
      roles: [],
      votes: []
    }
  }

  describe('MEMBERS_CREATED', () => {
    it('adds the new member', () => {
      const state = fromJS({ 1: generateMemberData(1) })

      const action = {
        type: MEMBERS_CREATED,
        payload: fromJS({ member: generateMemberData(2) })
      }

      const result = members(state, action).toJS()
      expect(result).to.deep.equal({
        1: generateMemberData(1),
        2: generateMemberData(2)
      })
    })
  })

  describe('MEMBERS_FETCHED', () => {
    it('adds the members', () => {
      const state = fromJS({ 1: generateMemberData(1) })

      const action = {
        type: MEMBERS_FETCHED,
        payload: fromJS([generateMemberData(2), generateMemberData(3)])
      }

      const result = members(state, action).toJS()
      expect(result).to.deep.equal({
        1: generateMemberData(1),
        2: generateMemberData(2),
        3: generateMemberData(3)
      })
    })
  })
})
