import React, { Component } from 'react'
import { connect, Provider } from 'react-redux'
import { fetchMember } from './redux/actions/memberActions'
import { USE_AUTH } from './config'

import Routes from './Routes.jsx'

class Root extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    document.addEventListener('sessionStarted', this.props.fetchMember, false)
    if (!USE_AUTH || localStorage.id_token != null) {
      this.props.fetchMember()
    }
  }

  componentDidUpdate() {}

  render() {
    const { store, history } = this.props
    return (
      <Provider store={store}>
        <Routes history={history} />
      </Provider>
    )
  }
}

const mapStateToProps = state => state
const mapDispatchToProps = dispatch => ({
  fetchMember: () => dispatch(fetchMember())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Root)
