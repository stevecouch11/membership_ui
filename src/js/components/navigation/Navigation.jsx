import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Navbar, Nav, NavItem } from 'react-bootstrap'
import { fetchMember } from '../../redux/actions/memberActions'
import { LinkContainer } from 'react-router-bootstrap'
import { isAdmin } from '../../services/members'
import _ from 'lodash'

const navMap = [
  {
    link: '/home',
    label: 'Home'
  },
  {
    link: '/my-membership',
    label: 'Membership'
  },
  {
    link: '/my-elections',
    label: 'Elections'
  },
  {
    link: '/my-committees',
    label: 'Committees'
  },
  {
    link: '/my-meetings',
    label: 'Meetings'
  },
  {
    link: '/my-contact-info',
    label: 'Contact Info'
  },
  {
    link: '/my-resources',
    label: 'Resources'
  }
]

class Navigation extends Component {
  componentWillReceiveProps(nextProps) {
    if (localStorage == null) {
      return
    }
    const authUser = localStorage.user
    if (authUser == null || authUser.email == null) {
      return
    }
    const dbMember = nextProps.member.getIn(['user', 'data'], null)
    if (
      (dbMember === null || dbMember.get('email_address') !== authUser.email) &&
      (!nextProps.member.getIn(['user', 'loading']) &&
        nextProps.member.getIn(['user', 'error']) === null)
    ) {
      this.props.fetchMember()
    }
  }

  render() {
    return (
      <Navbar className="mainNav" collapseOnSelect fixedTop fluid>
        <Navbar.Header>
          <Navbar.Brand>
            <div className="logo" />
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            {_.map(navMap, nav => (
              <LinkContainer to={nav.link} key={nav.label}>
                <NavItem className="dsaNavItem">{nav.label}</NavItem>
              </LinkContainer>
            ))}
            {isAdmin(this.props.member) && (
              <LinkContainer to="/admin">
                <NavItem className="dsaNavItem">Admin</NavItem>
              </LinkContainer>
            )}
          </Nav>

          <Nav pullRight className="right-nav">
            <LinkContainer to="/logout">
              <NavItem className="dsaNavItem">Logout</NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default connect(
  state => state,
  dispatch =>
    bindActionCreators(
      {
        fetchMember
      },
      dispatch
    )
)(Navigation)
