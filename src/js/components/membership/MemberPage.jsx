import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { membershipApi } from '../../services/membership'
import { HTTP_GET, HTTP_POST, logError } from '../../util/util'
import { fromJS, Map } from 'immutable'
import { Members } from '../../client/MemberClient'
import { fetchElections } from '../../redux/actions/electionActions'
import Loading from '../common/Loading'
import AddMeeting from '../admin/AddMeeting'
import AddRole from '../admin/AddRole'
import AddEligibleVoter from '../admin/AddEligibleVoter'
import EligibleVotes from './EligibleVotes'
import { Form, Glyphicon } from 'react-bootstrap'
import dateFormat from 'dateformat'
import { Fn } from '../../functional'

dateFormat.masks.dsaElection = "dddd, mmmm dS 'at' h:MM TT"

class MemberPageComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      member: null,
      inSubmission: false,
      meetingShortId: ''
    }
  }

  componentDidMount() {
    this.fetchMemberData()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.fetchMemberData()
    }
  }

  render() {
    if (this.state.member === null) {
      return <Loading />
    }

    return (
      <div>
        {this.renderMemberInfo()}
        {this.renderCommittees()}
        {this.renderMeetings()}
        {this.renderEligibleVoters()}
      </div>
    )
  }

  renderMemberInfo() {
    const memberData = this.state.member

    const doNotEmail = memberData.get('do_not_email')
    const doNotCall = memberData.get('do_not_call')

    return (
      <div>
        <h2>Member Info</h2>
        <p>
          {memberData.getIn(['info', 'first_name'])}{' '}
          {memberData.getIn(['info', 'last_name'])}
        </p>
        <p>Email address: {memberData.getIn(['info', 'email_address'])}</p>
        <p>
          Emails {doNotEmail ? 'disabled' : 'enabled'}, phone calls{' '}
          {doNotCall ? 'disabled' : 'enabled'}
        </p>
      </div>
    )
  }

  renderCommittees() {
    const roles = this.state.member
      .get('roles')
      .sortBy(role => role.get('committee'))
      .map((role, index) => {
        return (
          <div key={`role-${index}`} className="committee-member">
            <button onClick={e => this.removeRole(role)}>
              <Glyphicon glyph="remove" />
            </button>
            {`${role.get('committee')}: ${role.get('role')}`}
          </div>
        )
      })

    return (
      <div>
        <h2>Committees</h2>
        {roles}
        <AddRole
          memberId={
            this.props.params.memberId
              ? this.props.params.memberId
              : this.state.member.get('id')
          }
          refresh={() => this.fetchMemberData()}
        />
      </div>
    )
  }

  renderMeetings() {
    const meetings = this.state.member
      .get('meetings')
      .map((meeting, index) => (
        <div key={`meeting-${index}`}>{meeting.get('name')}</div>
      ))

    return (
      <div>
        <h2>Meetings attended</h2>
        <Form
          inline
          onSubmit={e => {
            this.attendMeeting(e)
          }}
        >
          <label htmlFor="meetingCode">Meeting Code: #</label>{' '}
          <input
            id="meetingCode"
            type="text"
            placeholder="0000"
            maxLength="4"
            value={Fn.falsyOr(id => `${id}`, this.state.meetingShortId)}
            onChange={e => {
              const meetingCode = e.target.value
              this.setState({ meetingShortId: meetingCode })
            }}
          />
          <button type="submit">Join</button>
        </Form>
        {meetings}
        <AddMeeting
          memberId={
            this.props.params.memberId
              ? this.props.params.memberId
              : this.state.member.get('id')
          }
          refresh={() => this.fetchMemberData()}
        />
      </div>
    )
  }

  renderEligibleVoters() {
    return (
      <div>
        <EligibleVotes
          votes={this.state.member.get('votes')}
          elections={this.props.elections.get('byId')}
        />
        <AddEligibleVoter
          memberId={
            this.props.params.memberId
              ? this.props.params.memberId
              : this.state.member.get('id')
          }
          refresh={() => this.fetchMemberData()}
        />
      </div>
    )
  }

  async fetchMemberData() {
    let member
    try {
      const results = await membershipApi(HTTP_GET, `/admin/member/details`, {
        member_id: this.props.params.memberId
      })
      member = fromJS(results)
    } catch (err) {
      return logError('Error loading member details', err)
    }
    this.setState({ member: member })
    this.props.fetchElections()
  }

  async removeRole(role) {
    const committee = this.props.committees
      .get('byId', Map())
      .find(committee => committee.get('name') === role.get('committee'))

    await Members.removeRole(
      role.get('member_id'),
      role.get('role'),
      committee.get('id')
    )
    this.fetchMemberData()
  }

  async attendMeeting(e) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const meeting = await membershipApi(HTTP_POST, '/meeting/attend', {
        meeting_short_id: this.state.meetingShortId,
        member_id: this.props.params.memberId
          ? this.props.params.memberId
          : this.state.member.get('id')
      })
      this.setState({ meetingShortId: '' })
      const landingUrl = fromJS(meeting).get('landing_url', null)
      if (landingUrl) {
        // TODO: Figure out how to inject this for testing
        location.href = landingUrl
      }
      this.fetchMemberData()
    } catch (err) {
      return logError('Error adding attendee', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({ fetchElections }, dispatch)
)(MemberPageComponent)
