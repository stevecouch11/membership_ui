import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Col, Grid, Row } from 'react-bootstrap'
import { isMemberLoaded } from '../../services/members'
import mymembership from '../../../../images/mymembership.png'
import myelections from '../../../../images/myelections.png'
import mycommittees from '../../../../images/mycommittees.png'
import mymeetings from '../../../../images/mymeetings.png'
import mycontactinfo from '../../../../images/mycontactinfo.png'
import myresources from '../../../../images/myresources.png'

class MainPage extends Component {
  render() {
    let welcomeMessage = <h2>Welcome!</h2>
    if (isMemberLoaded(this.props.member)) {
      const memberInfo = this.props.member.getIn(['user', 'data', 'info'])
      const fullName =
        memberInfo.get('first_name') + ' ' + memberInfo.get('last_name')

      welcomeMessage = <h1>Welcome, {fullName}!</h1>
    }

    return (
      <Grid>
        <Row>
          <Col xs={12}>{welcomeMessage}</Col>
        </Row>
        <Row>
          <Col xs={12} sm={6} md={4} lg={3}>
            {this.renderPanel('My Membership', '/my-membership', mymembership)}
          </Col>
          <Col xs={12} sm={6} md={4} lg={3}>
            {this.renderPanel('My Elections', '/my-elections', myelections)}
          </Col>
          <Col xs={12} sm={6} md={4} lg={3}>
            {this.renderPanel('My Committees', '/my-committees', mycommittees)}
          </Col>
          <Col xs={12} sm={6} md={4} lg={3}>
            {this.renderPanel('My Meetings', '/my-meetings', mymeetings)}
          </Col>
          <Col xs={12} sm={6} md={4} lg={3} lgOffset={3}>
            {this.renderPanel(
              'My Contact Info',
              '/my-contact-info',
              mycontactinfo
            )}
          </Col>
          <Col xs={12} sm={6} md={4} lg={3}>
            {this.renderPanel('My Resources', '/my-resources', myresources)}
          </Col>
        </Row>
      </Grid>
    )
  }

  renderPanel(title, path, imageUrl) {
    return (
      <Link to={path}>
        <div className="panel portal-panel">
          <div className="panel-heading text-center">
            <h3>{title}</h3>
          </div>
          <div className="panel-body">
            <img className="img-responsive" src={imageUrl} alt={title} />
          </div>
        </div>
      </Link>
    )
  }
}

export default connect(state => state)(MainPage)
