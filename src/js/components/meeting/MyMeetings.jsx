import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Grid } from 'react-bootstrap'
import { Link } from 'react-router'
import { bindActionCreators } from 'redux'
import { isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'
import { fetchAllMeetings } from '../../redux/actions/meetingActions'

class MyMeetings extends Component {
  componentDidMount() {
    this.props.fetchAllMeetings()
  }

  componentDidUpdate(prevProps) {
    const prevUser = prevProps.member.getIn(['user', 'data'])
    const currentUser = this.props.member.getIn(['user', 'data'])

    if (prevUser !== currentUser) {
      this.props.fetchAllMeetings()
    }
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    return (
      <Grid>
        <h1>My Meetings</h1>
        <h3>What meetings have I attended?</h3>
        {this.renderAttendance()}
        <h3>Where can I find the rest of the meetings?</h3>
        <p>
          Check out our <Link to={`/meetings`}>full list of meetings</Link>.
        </p>
      </Grid>
    )
  }

  renderAttendance() {
    const memberData = this.props.member.getIn(['user', 'data'])

    const meetings = memberData
      .get('meetings')
      .reverse()
      .map(meeting => {
        const meetingId = meeting.get('meeting_id')
        const meetingName = meeting.get('name')
        const landingUrl = this.props.meetings.getIn([
          'byId',
          meetingId,
          'landing_url'
        ])

        if (landingUrl == null) {
          return <div key={`meeting-${meetingId}`}>{meetingName}</div>
        } else {
          return (
            <div key={`meeting-${meetingId}`}>
              <a href={landingUrl}>{meetingName}</a>
            </div>
          )
        }
      })

    if (meetings.size === 0) {
      return (
        <p>
          You haven't attended any chapter meetings yet. To find our upcoming
          events, check out
          <a href="https://dsasf.org/events">dsasf.org/events</a>.
        </p>
      )
    } else {
      return (
        <div>
          <p>You've attended the following meetings so far:</p>
          {meetings}
        </div>
      )
    }
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({ fetchAllMeetings }, dispatch)
)(MyMeetings)
