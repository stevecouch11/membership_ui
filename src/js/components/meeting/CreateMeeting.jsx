import React, { Component } from 'react'
import { connect } from 'react-redux'
import FieldGroup from '../common/FieldGroup'
import { Button, Col, Form, Row } from 'react-bootstrap'
import { List, Map } from 'immutable'
import { bindActionCreators } from 'redux'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import * as committees from '../../redux/actions/committeeActions'
import * as meetings from '../../redux/actions/meetingActions'

class CreateMeeting extends Component {
  constructor(props) {
    super(props)
    this.state = {
      newMeeting: Map({
        name: '',
        committeeId: '',
        landingUrl: '',
        startTime: null,
        endTime: null
      })
    }
  }

  componentWillMount() {
    this.props.fetchCommittees()
  }

  canCreateMeeting() {
    return (
      this.state.newMeeting.get('name', '').length > 0 &&
      !this.props.meetings.getIn(['form', 'create', 'inSubmission'], false)
    )
  }

  render() {
    const isGeneralAdmin = isAdmin(this.props.member)
    if (!isGeneralAdmin && !isCommitteeAdmin(this.props.member)) {
      return <div />
    }

    const committeeAdminIds = this.props.member
      .getIn(['user', 'data', 'roles'], List())
      .filter(role => role.get('role') === 'admin' && role.get('committee_id'))
      .map(role => role.get('committee_id'))
      .toSet()

    let committeesById = this.props.committees.get('byId', Map())
    if (!isGeneralAdmin) {
      committeesById = committeesById.filter(committee =>
        committeeAdminIds.has(committee.get('id'))
      )
    }
    const committeeIdsByName = committeesById
      .mapEntries(([cid, c]) => [c.get('name'), cid])
      .set('', null) // add the empty option
      .sort()

    return (
      <div>
        <Row>
          <Col sm={4}>
            <h2>Create Meeting</h2>
            <Form horizontal onSubmit={e => e.preventDefault()}>
              <FieldGroup
                formKey="name"
                type="text"
                label="Meeting"
                value={this.state.newMeeting.get('name')}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
                required
              />
              <FieldGroup
                formKey="committeeId"
                componentClass="select"
                options={committeeIdsByName.keySeq().toJS()}
                optionMap={committeeIdsByName.toJS()}
                label="Committee"
                value={this.state.newMeeting.get('committeeId')}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
              />
              <FieldGroup
                formKey="landingUrl"
                type="text"
                label="Landing URL"
                value={this.state.newMeeting.get('landingUrl')}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
              />
              <FieldGroup
                formKey="startTime"
                componentClass="datetime"
                label="Start Time"
                value={this.state.newMeeting.get('startTime')}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
              />
              <FieldGroup
                formKey="endTime"
                componentClass="datetime"
                label="End Time"
                value={this.state.newMeeting.get('endTime')}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: this.state.newMeeting.set(formKey, value)
                  })
                }
              />
              <button
                type="submit"
                disabled={!this.canCreateMeeting()}
                onClick={() => this.props.createMeeting(this.state.newMeeting)}
              >
                Create Meeting
              </button>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }
}

export default connect(
  state => state,
  dispatch =>
    bindActionCreators(
      {
        createMeeting: meetings.createMeeting,
        fetchCommittees: committees.fetchCommittees
      },
      dispatch
    )
)(CreateMeeting)
