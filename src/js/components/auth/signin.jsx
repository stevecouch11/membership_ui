import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../redux/actions/auth'
import PropTypes from 'prop-types'

class Signin extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  componentWillMount() {
    const urlParams = new URLSearchParams(window.location.search)
    const redirect = urlParams.get('redirect')
    this.props.signoutUser()
    this.props.signinUser(redirect)
  }

  componentWillUpdate(nextProps, nextState) {
    const { error } = nextProps
    if (error) {
      this.container.error(`${error}`, 'Signin Failed', {
        timeOut: 3000,
        extendedTimeOut: 4000
      })
      nextProps.cleardown()
    }
  }

  handleClick() {
    this.props.signinUser()
  }

  render() {
    return (
      <div>
        <p>Welcome to DSA SF's member portal. Please sign in.</p>
        <button onClick={this.handleClick}>Sign In</button>
      </div>
    )
  }
}

const mapStateToProps = state => state

export default connect(
  mapStateToProps,
  actions
)(Signin)
