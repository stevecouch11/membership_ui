import React, { Component } from 'react'
import { connect } from 'react-redux'
import { membershipApi } from '../../services/membership'
import FieldGroup from '../common/FieldGroup'
import { HTTP_GET, HTTP_POST, logError } from '../../util/util'
import { Button, Col, Form, Row } from 'react-bootstrap'
import { fromJS, Map } from 'immutable'
import { fetchCommittees } from '../../redux/actions/committeeActions'

class AddRole extends Component {
  constructor(props) {
    super(props)
    this.state = {
      newRole: Map({
        member_id: this.props.memberId,
        committee_id: '0',
        role: 'member'
      }),
      inSubmission: false
    }
  }

  componentDidMount() {
    // TODO(jesse): hide the form behind an edit button to save bandwith for admins
    this.props.fetchCommittees()
  }

  updateForm(name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    const update = this.state[name].set(formKey, value)
    this.setState({ [name]: update })
  }

  render() {
    return (
      <div>
        <Row>
          <Col sm={4}>
            <Form horizontal onSubmit={e => e.preventDefault()}>
              <h3>Add Role</h3>
              <FieldGroup
                formKey="role"
                componentClass="select"
                options={['admin', 'member']}
                label="Role"
                value={this.state.newRole.get('role')}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('newRole', formKey, value)
                }
                required
              />
              <FieldGroup
                formKey="committee_id"
                componentClass="select"
                label="Committee"
                options={this.getCommittees()}
                optionMap
                value={this.state.newRole.get('committee_id')}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('newRole', formKey, value)
                }
                required
              />
              <button
                type="submit"
                onClick={e => this.submitForm(e, 'newRole', '/member/role')}
              >
                Add Role
              </button>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }

  getCommittees() {
    return this.props.committees
      .get('byId', Map())
      .mapEntries(([id, data]) => [id, data.get('name')])
      .set(0, 'General')
  }

  async submitForm(e, name, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[name])
      this.props.refresh()
    } catch (err) {
      return logError('Error loading test', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect(
  state => state,
  dispatch => ({
    fetchCommittees: () => dispatch(fetchCommittees())
  })
)(AddRole)
