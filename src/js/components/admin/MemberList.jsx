import React, { Component } from 'react'
import FieldGroup from '../common/FieldGroup'
import { isAdmin } from '../../services/members'
import { connect } from 'react-redux'
import { Col, Form, Row } from 'react-bootstrap'
import {
  createMember,
  fetchAllMembers
} from '../../redux/actions/memberActions'
import MemberSearch from './MemberSearch'

class MemberList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      newMember: {
        first_name: '',
        last_name: '',
        email_address: '',
        give_chapter_member_role: true
      },
      admin: { email_address: '', committee: '' },
      inSubmission: false
    }
  }

  componentWillUpdate() {
    const { member, router } = this.props
    if (!isAdmin(member)) {
      router.push('/home')
    }
  }

  updateForm(name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({ [name]: update })
  }

  render() {
    if (!isAdmin(this.props.member)) {
      return <div />
    }

    return (
      <div>
        <Row>
          <Col sm={4}>
            <h2>Add Member</h2>
            <Form horizontal onSubmit={e => e.preventDefault()}>
              <FieldGroup
                formKey="first_name"
                type="text"
                label="First Name"
                value={this.state.newMember.first_name}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('newMember', formKey, value)
                }
                required
              />
              <FieldGroup
                formKey="last_name"
                type="text"
                label="Last Name"
                value={this.state.newMember.last_name}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('newMember', formKey, value)
                }
                required
              />
              <FieldGroup
                formKey="email_address"
                type="text"
                label="Email"
                value={this.state.newMember.email_address}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('newMember', formKey, value)
                }
                required
              />
              <FieldGroup
                componentClass="checkbox"
                formKey="give_chapter_member_role"
                label="Is member of DSA National"
                value={this.state.newMember.give_chapter_member_role}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('newMember', formKey, value)
                }
              />
              <button
                type="submit"
                onClick={e => {
                  e.stopPropagation()
                  this.props.createMember(this.state.newMember)
                }}
              >
                Add Member
              </button>
            </Form>
          </Col>
        </Row>
        <MemberSearch />
      </div>
    )
  }
}

export default connect(
  state => state,
  dispatch => ({
    createMember: member => dispatch(createMember(member)),
    fetchAllMembers: () => dispatch(fetchAllMembers())
  })
)(MemberList)
