import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Grid } from 'react-bootstrap'
import { isAdmin } from '../../services/members'
import { importRoster } from '../../redux/actions/memberActions'

class ImportRoster extends Component {
  render() {
    if (!isAdmin(this.props.member)) {
      return null
    }

    return (
      <Grid>
        <h1>Import Roster</h1>
        <p>
          Upload the chapter roster as a CSV file. Make sure dates are in
          "m/d/yy HH:MM" format.
        </p>
        <p>
          <input
            name="file"
            type="file"
            onChange={e => this.setState({ file: e.target.files[0] })}
          />
        </p>
        <p>
          <button
            type="submit"
            onClick={() => this.submit()}
            disabled={this.props.memberImport.get('state') === 'initiated'}
          >
            Import
          </button>
        </p>
        {this.renderResult()}
      </Grid>
    )
  }

  renderResult() {
    switch (this.props.memberImport.get('state')) {
      case 'initiated':
        return <p>Importing...</p>
      case 'success':
        const importResults = this.props.memberImport.get('results')
        return (
          <div>
            <p>Import complete!</p>
            <ul>
              <li>
                {importResults.get('members_created')} members added,{' '}
                {importResults.get('members_updated')} members updated.
              </li>
              <li>
                {importResults.get('memberships_created')} memberships added.
              </li>
              <li>
                {importResults.get('identities_created')} identities added.
              </li>
              <li>
                {importResults.get('phone_numbers_created')} phone numbers
                added.
              </li>
              <li>{importResults.get('member_roles_added')} roles added.</li>
              <li>{importResults.get('member_roles_added')} roles added.</li>
            </ul>
          </div>
        )
      case 'failure':
        return <p>Import failed.</p>
      default:
        return null
    }
  }

  submit() {
    this.props.importRoster(this.state.file)
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({ importRoster }, dispatch)
)(ImportRoster)
