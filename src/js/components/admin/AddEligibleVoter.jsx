import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { membershipApi } from '../../services/membership'
import FieldGroup from '../common/FieldGroup'
import { HTTP_POST, logError } from '../../util/util'
import { Button, Col, Form, Row } from 'react-bootstrap'
import { Map } from 'immutable'
import { fetchElections } from '../../redux/actions/electionActions'

class AddEligibleVoter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      eligibleVoter: Map({ member_id: this.props.memberId, election_id: '' }),
      inSubmission: false
    }
  }

  updateForm(name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    const update = this.state[name].set(formKey, value)
    this.setState({ [name]: update })
  }

  render() {
    const electionOptions = Map(
      this.props.elections.get('byId').map(election => election.get('name'))
    )

    return (
      <div>
        <Row>
          <Col sm={4}>
            <Form horizontal onSubmit={e => e.preventDefault()}>
              <h3>Add Eligible Voter</h3>
              <FieldGroup
                formKey="election_id"
                componentClass="select"
                label="Election"
                options={electionOptions}
                placeHolder="Select an election"
                optionMap
                value={this.state.eligibleVoter.get('election_id')}
                onFormValueChange={(formKey, value) =>
                  this.updateForm('eligibleVoter', formKey, value)
                }
                required
              />
              <Button
                type="submit"
                onClick={e =>
                  this.submitForm(e, 'eligibleVoter', '/election/voter')
                }
              >
                Add Voter
              </Button>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }

  async submitForm(e, name, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[name])
      this.props.refresh()
    } catch (err) {
      return logError('Error loading test', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({ fetchElections }, dispatch)
)(AddEligibleVoter)
