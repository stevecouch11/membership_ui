import React, { Component } from 'react'
import { debounce } from 'lodash'
import { InputGroup, FormControl } from 'react-bootstrap'

class SearchInput extends Component {
  constructor(props) {
    super(props)
    this.handleSearchChange = this.handleSearchChange.bind(this)
    this.queryMembers = debounce(this.queryMembers.bind(this), 600)
  }
  handleSearchChange({ target }) {
    this.props.onChange(target.value)
    this.queryMembers()
  }
  queryMembers() {
    if (!this.props.isLoading && this.props.query !== '') {
      this.props.onSearch(this.props.query)
    }
  }
  render() {
    const { query, onClear } = this.props
    return (
      <div>
        <InputGroup>
          <FormControl
            type="text"
            value={query}
            onChange={this.handleSearchChange}
            placeholder="Search users..."
          />
          <InputGroup.Addon
            className={`clear-member-search ${query && 'can-clear'}`}
            onClick={onClear}
          >
            x
          </InputGroup.Addon>
        </InputGroup>
      </div>
    )
  }
}

export default SearchInput
