import dateFormat from 'dateformat'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { electionStatus } from '../../services/elections'

dateFormat.masks.dsaElection = "dddd, mmmm dS 'at' h:MM TT"

class UpcomingElections extends Component {
  render() {
    const allVotes = this.props.member.getIn(['user', 'data', 'votes'])
    const futureElections = allVotes
      .map(vote => this.getElection(vote.get('election_id')))
      .filter(election => election != null)
      .filter(election => this.isFutureElection(election))

    if (futureElections.size === 0) {
      return <p>There are no upcoming elections scheduled for you.</p>
    } else {
      return (
        <div>
          {futureElections.map(election => this.renderElection(election))}
        </div>
      )
    }
  }

  renderElection(election) {
    const startTime = new Date(election.get('voting_begins_epoch_millis'))

    return (
      <p key={`election-${election.get('id')}`}>
        Voting on{' '}
        <strong>
          <Link to={`/elections/${election.get('id')}`}>
            {election.get('name')}
          </Link>
        </strong>{' '}
        begins {dateFormat(startTime, 'dsaElection')}.
      </p>
    )
  }

  getElection(electionId) {
    return this.props.elections.getIn(['byId', electionId])
  }

  isFutureElection(election) {
    return electionStatus(election) === 'polls not open'
  }
}

export default connect(state => state)(UpcomingElections)
