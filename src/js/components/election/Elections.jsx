import dateFormat from 'dateformat'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { membershipApi } from '../../services/membership'
import { fetchElections } from '../../redux/actions/electionActions'
import { isAdmin } from '../../services/members'
import FieldGroup from '../common/FieldGroup'
import { HTTP_POST, logError } from '../../util/util'
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormGroup,
  Grid,
  Row
} from 'react-bootstrap'
import DateTime from 'react-datetime'
import { Link } from 'react-router'

dateFormat.masks.dsaElectionRange = 'mmmm d, yyyy'

class Elections extends Component {
  constructor(props) {
    super(props)
    this.state = {
      election: { name: '', candidate_list: [], number_winners: '1' },
      inSubmission: false
    }
  }

  componentDidMount() {
    this.props.fetchElections()
  }

  updateForm(name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]

    if (formKey === 'candidate_list') {
      update[formKey] = value.split(',').map(s => s.trim())
    } else {
      update[formKey] = value
    }

    this.setState({ [name]: update })
  }

  render() {
    const admin = isAdmin(this.props.member)
    const elections = this.props.elections
      .get('byId')
      .valueSeq()
      .sortBy(election => -election.get('id'))
    const electionRows = elections.map(election => {
      const id = election.get('id')
      const displayStartTime =
        election.get('voting_begins_epoch_millis') !== null
      const displayEndTime =
        displayStartTime && election.get('voting_begins_epoch_millis') !== null
      const startTime =
        displayStartTime && new Date(election.get('voting_begins_epoch_millis'))
      const endTime =
        displayEndTime && new Date(election.get('voting_ends_epoch_millis'))

      return (
        <Row key={`election-toolbox-${id}`} className="election-toolbox">
          <Col sm={6} md={3}>
            <Link to={`/elections/${id}/`}>{election.get('name')}</Link>
          </Col>
          <Col sm={6}>
            {displayStartTime && dateFormat(startTime, 'mediumDate')}
            {displayEndTime && ' - ' + dateFormat(endTime, 'mediumDate')}
          </Col>
        </Row>
      )
    })
    return (
      <Grid>
        {admin && (
          <Row>
            <Col sm={8} lg={6}>
              <h2>Create Election</h2>
              <Form horizontal onSubmit={e => e.preventDefault()}>
                <FieldGroup
                  formKey="name"
                  type="text"
                  label="Election Name"
                  value={this.state.election.name}
                  onFormValueChange={(formKey, value) =>
                    this.updateForm('election', formKey, value)
                  }
                  required
                />
                <FieldGroup
                  formKey="start_time"
                  componentClass="datetime"
                  label="Start Time"
                  value={this.state.election.start_time}
                  onFormValueChange={(formKey, value) =>
                    this.updateForm('election', formKey, value.toISOString())
                  }
                />
                <FieldGroup
                  formKey="end_time"
                  componentClass="datetime"
                  label="End Time"
                  value={this.state.election.end_time}
                  onFormValueChange={(formKey, value) =>
                    this.updateForm('election', formKey, value.toISOString())
                  }
                />
                <FieldGroup
                  formKey="number_winners"
                  type="number"
                  step="1"
                  min="1"
                  label="Number of winners"
                  value={this.state.election.number_winners}
                  onFormValueChange={(formKey, value) =>
                    this.updateForm('election', formKey, value)
                  }
                  required
                />
                <FieldGroup
                  formKey="candidate_list"
                  type="text"
                  label="Candidate Names (comma-separated)"
                  onFormValueChange={(formKey, value) =>
                    this.updateForm('election', formKey, value)
                  }
                  required
                />
                <FieldGroup
                  formKey="description"
                  componentClass="textarea"
                  rows="5"
                  type="text"
                  label="Description (truncated to 2048 characters)"
                  onFormValueChange={(formKey, value) =>
                    this.updateForm('election', formKey, value)
                  }
                />
                <FieldGroup
                  formKey="description_img"
                  rows="1"
                  type="text"
                  label="Link to a description image"
                  onFormValueChange={(formKey, value) =>
                    this.updateForm('election', formKey, value)
                  }
                />
                <button
                  type="submit"
                  onClick={e => this.submitForm(e, 'election', '/election')}
                >
                  Add Election
                </button>
              </Form>
            </Col>
          </Row>
        )}
        <h2> Elections </h2>
        {electionRows}
      </Grid>
    )
  }

  async submitForm(e, name, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return Promise()
    }
    this.setState({ inSubmission: true })
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[name])
      this.props.fetchElections()
    } catch (err) {
      return logError('Error creating election', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect(
  state => state,
  dispatch => bindActionCreators({ fetchElections }, dispatch)
)(Elections)
