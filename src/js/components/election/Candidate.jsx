import React, { Component } from 'react'

export default class Candidate extends Component {
  render() {
    return (
      <div>
        {this.getIcon()}
        <span style={{ fontSize: 36 }}>{this.props.name}</span>
      </div>
    )
  }

  getIcon() {
    if (this.props.imageUrl) {
      return (
        <img
          src={this.props.imageUrl}
          style={{
            width: 'auto',
            height: this.props.height,
            marginRight: 30
          }}
          alt={this.props.name}
        />
      )
    } else {
      return null
    }
  }
}
