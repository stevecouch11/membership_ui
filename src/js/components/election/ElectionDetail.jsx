import dateFormat from 'dateformat'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Grid } from 'react-bootstrap'
import { List, Map } from 'immutable'
import { logError } from '../../util/util'
import { eligibilityEmail } from '../../services/emails'
import { Elections } from '../../client/ElectionClient'
import { Members } from '../../client/MemberClient'
import { eligibleVoteStatus, electionStatus } from '../../services/elections'
import { isAdmin } from '../../services/members'
import Candidate from './Candidate'
import ElectionResults from './ElectionResults'

dateFormat.masks.dsaElection = "dddd, mmmm dS 'at' h:MM TT"

class ElectionDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      member: Map(),
      election: Map({
        name: '...',
        status: '...',
        candidates: List(),
        number_winners: '...',
        transitions: List(),
        description_img: '...'
      }),
      eligible: List(),
      inSubmission: false,
      results: Map()
    }
  }

  componentDidMount() {
    this.getMemberDetails()
    this.getElectionDetails()
    if (isAdmin(this.props.member)) {
      this.getEligibleVoters()
    }
  }

  updateForm(name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({ [name]: update })
  }

  render() {
    const candidates = this.state.election
      .get('candidates')
      .map(candidate => (
        <Candidate
          key={candidate.get('id')}
          imageUrl={candidate.get('image_url')}
          name={candidate.get('name')}
          height={60}
        />
      ))

    if (this.state)
      return (
        <Grid>
          <h1>{this.state.election.get('name')}</h1>
          <h3>Election Details</h3>
          <p className="text-pre-wrap">
            {this.state.election.get('description')}
          </p>
          {this.renderElectionStatusSummary()}
          {this.renderVoterStatusSummary()}
          {this.renderEligibleVoterSummary()}
          {this.renderVoteDescriptionImage()}
          {this.renderCandidateCountSummary()}
          <h2>Candidates</h2>
          {candidates}
          {this.renderAdminTools()}
        </Grid>
      )
  }

  renderElectionStatusSummary() {
    const status = electionStatus(this.state.election)
    const startTime = this.state.election.get('voting_begins_epoch_millis')
    const endTime = this.state.election.get('voting_ends_epoch_millis')

    let electionStatusText = status
    if (status === 'polls not open') {
      electionStatusText = (
        <span>Voting will begin {dateFormat(startTime, 'dsaElection')}.</span>
      )
    } else if (status === 'polls closed') {
      electionStatusText = (
        <span>
          Voting closed {dateFormat(endTime, 'dsaElection')}, and we're waiting
          for the final count.
        </span>
      )
    } else if (status === 'final') {
      electionStatusText = (
        <span>
          Voting closed {dateFormat(endTime, 'dsaElection')}, and the ballots
          have been counted.
        </span>
      )
    } else if (status === 'polls open') {
      electionStatusText = endTime ? (
        <span>Voting is open!</span>
      ) : (
        <span>Voting is open until {dateFormat(endTime, 'dsaElection')}.</span>
      )
    } else if (status === 'draft') {
      electionStatusText = <span>This election has not been published.</span>
    } else if (status === 'canceled') {
      electionStatusText = <span>This election was canceled.</span>
    }

    return (
      <p>
        <b>Election status:</b> {electionStatusText}
      </p>
    )
  }

  renderVoterStatusSummary() {
    const { electionId } = this.props.params

    const vote = (this.state.member
      ? this.state.member.get('votes', List())
      : List()
    ).find(
      vote => vote.get('election_id', '').toString() === electionId.toString()
    )
    const voteStatus = eligibleVoteStatus(
      this.state.election.set('id', electionId),
      vote
    )
    const status = electionStatus(this.state.election)

    let eligibilityText = voteStatus
    switch (voteStatus) {
      case 'ineligible':
        if (status === 'draft' || status === 'polls not open') {
          eligibilityText = (
            <span>
              You have not been marked as eligible for this vote. Voting has not
              yet begun, so you may be added soon. If you have questions about
              this vote, contact {eligibilityEmail()}.
            </span>
          )
        } else if (status === 'polls open') {
          eligibilityText = (
            <span>
              You are not listed as eligible to vote. If you believe this is a
              mistake, contact {eligibilityEmail()}.
            </span>
          )
        } else {
          eligibilityText = 'You were ineligible for this vote.'
        }
      case 'already voted':
        if (status === 'polls closed' || status === 'final') {
          eligibilityText =
            'You were eligible for this vote, and your ballot was counted.'
        } else {
          eligibilityText =
            'You are eligible to vote, and your ballot has been counted.'
        }
      case 'eligible':
        if (status === 'draft' || status === 'polls not open') {
          eligibilityText = 'You will be eligible to vote.'
        } else if (status === 'polls open') {
          eligibilityText = 'You are eligible to vote.'
        } else {
          eligibilityText = 'You were eligible to vote.'
        }
    }

    return (
      <p>
        <b>Voter Status:</b> {eligibilityText}{' '}
        <b>
          {voteStatus === 'eligible' &&
            electionStatus === 'polls open' && (
              <a href={`/vote/${electionId}`}>Vote Now!</a>
            )}
        </b>
      </p>
    )
  }

  renderEligibleVoterSummary() {
    if (this.state.eligible.size > 0) {
      return (
        <p>
          <b>Eligible Voters:</b> {this.state.eligible.size} members are
          eligible for this vote.
        </p>
      )
    } else {
      return null
    }
  }

  renderVoteDescriptionImage() {
    if (!this.state.election.get('description_img')) {
      return null
    }

    return (
      <p>
        <img
          style={{
            maxWidth: '100%',
            maxHeight: '100%'
          }}
          src={this.state.election.get('description_img')}
        />
      </p>
    )
  }

  renderCandidateCountSummary() {
    return (
      <p>
        <b>Number of positions:</b> This election is to select{' '}
        {this.state.election.get('number_winners')} out of{' '}
        {this.state.election.get('candidates').size} candidates.
      </p>
    )
  }

  renderAdminTools() {
    if (!isAdmin(this.props.member)) {
      return null
    }

    const { electionId } = this.props.params

    return (
      <div>
        <h2>Manage Election</h2>
        <Link to={`/elections/${electionId}/edit`}>
          <button>Edit Election</button>
        </Link>
        <Link to={`/elections/${electionId}/print`}>
          <button>Print Ballots</button>
        </Link>
        <Link
          key={`enter-ballots-${electionId}`}
          to={`/admin/elections/${electionId}/vote`}
        >
          <button>Enter Ballots</button>
        </Link>
        <Link to={`/elections/${electionId}/signin`}>
          <button>Sign In Kiosk</button>
        </Link>
        <h2>Results</h2>
        <ElectionResults params={this.props.params} />
      </div>
    )
  }

  async getElectionDetails() {
    try {
      const results = await Elections.getElection(this.props.params.electionId)
      this.setState({ election: results })
    } catch (err) {
      return logError('Error fetching election', err)
    }
  }

  async getEligibleVoters() {
    try {
      const results = await Elections.getEligibleVoters(
        this.props.params.electionId
      )
      this.setState({ eligible: results || List() })
    } catch (err) {
      return logError('Error fetching eligible voters', err)
    }
  }

  // TODO: Move this to redux
  async getMemberDetails() {
    try {
      const member = await Members.getCurrentUser()
      this.setState({ member })
    } catch (err) {
      return logError('Error loading member details', err)
    }
  }
}

export default connect(state => state)(ElectionDetail)
