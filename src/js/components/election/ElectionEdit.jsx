import React, { Component } from 'react'
import { connect } from 'react-redux'
import DateTime from 'react-datetime'
import { isAdmin } from '../../services/members'
import { Elections } from '../../client/ElectionClient'
import { logError } from '../../util/util'
import { Col, ControlLabel, Form, FormGroup } from 'react-bootstrap'
import { fromJS, List, Map } from 'immutable'
import Candidate from './Candidate'

class ElectionEdit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      election: Map({
        id: 1,
        name: '',
        candidates: List(),
        number_winners: 1,
        transitions: List(),
        start_time: 1,
        end_time: 1,
        voting_begins_epoch_millis: 1,
        voting_ends_epoch_millis: 1
      }),
      inSubmission: false
    }
  }

  componentDidMount() {
    this.getElectionDetails()
  }

  updateForm(name, formKey, value) {
    if (this.state.inSubmission) {
      return
    }
    let update = this.state[name]
    update[formKey] = value
    this.setState({ [name]: update })
  }

  updateStartTime(startTime) {
    console.log('update start')
    if (this.state.inSubmission) {
      return
    }

    this.setState({
      election: this.state.election.set(
        'voting_begins_epoch_millis',
        new Date(startTime).getTime()
      )
    })
  }

  updateEndTime(endTime) {
    if (this.state.inSubmission) {
      return
    }

    this.setState({
      election: this.state.election.set(
        'voting_ends_epoch_millis',
        new Date(endTime).getTime()
      )
    })
  }

  async updateElection(e) {
    e.preventDefault()

    if (this.state.inSubmission) {
      return
    }
    this.setState({
      inSubmission: true
    })

    try {
      const attributeNames = [
        'voting_begins_epoch_millis',
        'voting_ends_epoch_millis'
      ]
      let updatedAttributes = this.state.election
      updatedAttributes = updatedAttributes
        // filter out attribute names we don't want to send up
        .filter((_, key) => attributeNames.includes(key))
        // treat falsy values as null when sending up
        .map((val, _) => val || null)

      const startTime = updatedAttributes.get('voting_begins_epoch_millis')
      const endTime = updatedAttributes.get('voting_ends_epoch_millis')
      if (startTime) {
        // rename attribute & convert for api requirements
        updatedAttributes = updatedAttributes.set(
          'start_time',
          new Date(startTime).toISOString()
        )
      }
      if (endTime) {
        // rename attribute & convert for api requirements
        updatedAttributes = updatedAttributes.set(
          'end_time',
          new Date(endTime).toISOString()
        )
      }

      const result = await Elections.updateElection(
        this.props.params.electionId,
        updatedAttributes
      )
      // parse received ISO 8601 strings
      let election = result.get('election')
      const updatedElection = this.state.election.merge(election)
      this.setState({ election: updatedElection })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  render() {
    const admin = isAdmin(this.props.member)
    const candidates = this.state.election
      .get('candidates')
      .map(candidate => (
        <Candidate
          key={candidate.get('id')}
          imageUrl={candidate.get('image_url')}
          name={candidate.get('name')}
          height={60}
        />
      ))

    const transitions = this.state.election
      .get('transitions', List())
      .map(t => (
        <li
          key={t}
          style={{
            display: 'inline-block',
            marginRight: 10
          }}
        >
          <button
            type="submit"
            value={t}
            onClick={e => this.submitTransition(e)}
          >
            {t}
          </button>
        </li>
      ))

    return admin ? (
      <div>
        <h1>
          {this.state.election.get('name')} ({this.state.election.get('status')}
          )
        </h1>
        <h3>Edit Election</h3>
        <Form horizontal onSubmit={e => e.preventDefault()}>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              Start Time
            </Col>
            <Col sm={6}>
              <DateTime
                value={this.state.election.get('voting_begins_epoch_millis')}
                onChange={m => this.updateStartTime(m)}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              End Time
            </Col>
            <Col sm={6}>
              <DateTime
                value={this.state.election.get('voting_ends_epoch_millis')}
                onChange={m => this.updateEndTime(m)}
              />
            </Col>
          </FormGroup>
          <button type="submit" onClick={e => this.updateElection(e)}>
            Submit
          </button>
        </Form>
        <h3>Update Election Status</h3>
        <ul
          style={{
            listStyleType: 'none',
            paddingLeft: 0
          }}
        >
          {transitions}
        </ul>
        <h3>
          Number of positions {this.state.election.get('number_winners')}{' '}
        </h3>
        {candidates}
      </div>
    ) : (
      <div />
    )
  }

  async getElectionDetails() {
    try {
      const results = await Elections.getElection(this.props.params.electionId)
      this.setState({ election: fromJS(results) })
    } catch (err) {
      return logError('Error fetching election', err)
    }
  }

  async submitTransition(e) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    const transition = e.target.value
    const confirmation = confirm(
      `Are you sure you want to ${transition} the election?`
    )
    if (!confirmation) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const results = await Elections.submitTransition(
        this.props.params.electionId,
        transition
      )
      const newElection = this.state.election
        .set('status', results.get('election_status'))
        .set('transitions', results.get('transitions'))

      this.setState({ election: newElection })
    } catch (err) {
      return logError('Error changing election state', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect(state => state)(ElectionEdit)
