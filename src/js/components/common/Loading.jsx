import React, { Component } from 'react'
import { Glyphicon } from 'react-bootstrap'

export default class Loading extends Component {
  render() {
    return (
      <div className="container text-center">
        <h1>Loading...</h1>
        <Glyphicon glyph="refresh" className="loading-wheel" />
      </div>
    )
  }
}
