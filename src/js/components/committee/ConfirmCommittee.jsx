import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Form } from 'react-bootstrap'
import { Map } from 'immutable'
import {
  fetchCommittees,
  requestCommitteeMembership
} from '../../redux/actions/committeeActions'
import FieldGroup from '../common/FieldGroup'

class ConfirmCommittee extends Component {
  componentDidMount() {
    this.props.fetchCommittees()
  }

  render() {
    const memberCommittees = this.props.member
      .getIn(['user', 'data', 'roles'])
      .filter(role => role.get('role') === 'active')
      .map(role => role.get('committee'))
      .filter(committee => committee !== 'general')

    const committeeIdsByName = this.props.committees
      .get('byId', Map())
      .mapEntries(([cid, c]) => [c.get('name'), cid])
      .filter((_, c) => !memberCommittees.includes(c))
      .set('', null) // add the empty option
      .sort()

    return (
      <Form horizontal>
        <FieldGroup
          formKey="committeeId"
          componentClass="select"
          options={committeeIdsByName.keySeq().toJS()}
          optionMap={committeeIdsByName.toJS()}
          label="Committee"
          onFormValueChange={(formKey, value) => {
            this.committeeRequestId = value
          }}
        />
        <button
          type="button"
          onClick={() =>
            this.props.requestCommitteeMembership(this.committeeRequestId)
          }
        >
          Request confirmation
        </button>
      </Form>
    )
  }
}

export default connect(
  state => state,
  dispatch =>
    bindActionCreators(
      { fetchCommittees, requestCommitteeMembership },
      dispatch
    )
)(ConfirmCommittee)
