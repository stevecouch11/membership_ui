import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  fetchEmailTemplate,
  saveEmailTemplate
} from '../../redux/actions/emailTemplateActions'
import { fetchEmailAddresses } from '../../redux/actions/emailAddressActions'
import { fetchEmailTopics } from '../../redux/actions/emailTopicActions'
import { sendEmailTemplate } from '../../redux/actions/emailTopicActions'

class EmailTemplateDetails extends Component {
  componentWillMount() {
    this.props.fetchTemplate(this.props.params.templateId)
    this.props.fetchEmailAddresses()
    this.props.fetchEmailTopics()
  }

  save() {
    const template = this.state.template
    if (
      template.get('name') === '' ||
      template.get('subject') === '' ||
      template.get('body') === ''
    ) {
      // do nothing
    } else {
      const template = this.getTemplate()
      this.setState({
        template: template.setIn(['topic_id'], this.getSelectedTopicValue())
      })
      this.props.saveTemplate(template)
    }
  }

  send() {
    // consider restricting sending to only saved templates?
    this.props.sendEmailTemplate(
      this.getSelectedTopicValue(),
      this.getTemplate().get('id'),
      this.getSelectedEmailValue()
    )
  }

  handleNameChange(event) {
    const template = this.getTemplate()
    this.setState({ template: template.setIn(['name'], event.target.value) })
  }

  handleSubjectChange(event) {
    const template = this.getTemplate()
    this.setState({ template: template.setIn(['subject'], event.target.value) })
  }

  handleBodyChange(event) {
    const template = this.getTemplate()
    this.setState({ template: template.setIn(['body'], event.target.value) })
  }

  getSelectedTopicValue() {
    const select = document.getElementById('topicsSelect')
    return select.options[select.selectedIndex].value
  }

  setSelectedTopicValue() {
    const select = document.getElementById('topicsSelect')
    return select.options[select.selectedIndex].value
  }

  getSelectedEmailValue() {
    const select = document.getElementById('emailsSelect')
    const selected = select.options[select.selectedIndex]
    if (selected != undefined) {
      return selected.innerHTML
    }
  }

  render() {
    const template = this.getTemplate()
    if (template == null) {
      return <div />
    }
    const modifiedDate = this.getDate(template)
    const emails = this.getEmailOptions()
    const topics = this.getTopics(template)
    return (
      <div style={{ paddingLeft: '32px' }}>
        <h2>{template.get('name')}</h2>
        <br />
        Send From:
        <select id="emailsSelect" style={{ marginLeft: 10 }}>
          {emails}
        </select>
        <br />
        <br />
        Topic / Recipients:
        <select id="topicsSelect" style={{ marginLeft: 10 }}>
          {topics}
        </select>
        <br />
        <br />
        Title
        <br />
        <textarea
          name="title"
          rows="1"
          cols="20"
          value={template.get('name')}
          onChange={e => this.handleNameChange(e)}
        />
        <br />
        <br />
        Subject
        <br />
        <textarea
          name="subject"
          rows="1"
          cols="20"
          value={template.get('subject')}
          onChange={e => this.handleSubjectChange(e)}
        />
        <br />
        <br />
        Body
        <br />
        <textarea
          name="body"
          rows="15"
          cols="40"
          value={template.get('body')}
          onChange={e => this.handleBodyChange(e)}
        />
        <br />
        <br />
        Date modified: {modifiedDate}
        <br />
        <br />
        <button type="button" name="save" onClick={() => this.save()}>
          Save
        </button>
        <button
          style={{ marginLeft: '8px' }}
          type="button"
          name="send"
          onClick={() => this.send()}
        >
          Send Email
        </button>
        <br />
        <br />
      </div>
    )
  }

  getTemplate() {
    let template = null
    if (this.state != null && this.state.template != null) {
      template = this.state.template
    } else if (
      this.props.templates != null &&
      this.props.templates.get('template') != null
    ) {
      template = this.props.templates.get('template')
    }
    return template
  }

  getDate(template) {
    const updated = template.get('last_updated')
    return new Date(updated).toDateString()
  }

  getEmailOptions() {
    return this.props.emailAddresses
      .get('byId')
      .valueSeq()
      .map(address => {
        const email = address.get('email_address')
        return <option value={email}>{email}</option>
      })
  }

  getTopics(template) {
    const topics = this.props.topics.get('byId')
    return topics.valueSeq().map(topic => {
      const isSelected = topic.get('id') == template.get('topic_id')
      return (
        <option selected={isSelected} value={topic.get('id')}>
          {topic.get('name')}
        </option>
      )
    })
  }
}

export default connect(
  state => state,
  dispatch =>
    bindActionCreators(
      {
        saveTemplate: saveEmailTemplate,
        fetchTemplate: fetchEmailTemplate,
        fetchEmailAddresses,
        fetchEmailTopics,
        sendEmailTemplate
      },
      dispatch
    )
)(EmailTemplateDetails)
