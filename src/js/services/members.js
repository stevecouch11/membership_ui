import { List } from 'immutable'

export function isMemberLoaded(member) {
  return member && member.getIn(['user', 'data'], List()) != null
}

export function isMember(member) {
  if (!member) {
    return false
  }
  return member
    .getIn(['user', 'data', 'roles'], List())
    .some(
      role =>
        role.get('role') === 'member' && role.get('committee') === 'general'
    )
}

export function isAdmin(member) {
  if (!member) {
    return false
  }
  return member
    .getIn(['user', 'data', 'roles'], List())
    .some(
      role =>
        role.get('role') === 'admin' && role.get('committee') === 'general'
    )
}

export function isCommitteeAdmin(member) {
  if (!member) {
    return false
  }
  return member
    .getIn(['user', 'data', 'roles'], List())
    .some(
      role =>
        role.get('role') === 'admin' &&
        role.get('committee') &&
        role.get('committee') !== 'general'
    )
}

const members = {
  isMemberLoaded,
  isAdmin,
  isCommitteeAdmin
}

export default members
