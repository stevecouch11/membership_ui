import { Api } from '../client/ApiClient'

export class EmailTemplateClient {
  constructor(api) {
    this.api = api
  }

  all() {
    return this.api
      .url('/email_templates')
      .get()
      .execute()
  }

  template(id) {
    return this.api
      .url(`/email_template/${id}`, id)
      .get()
      .execute()
  }

  save(template) {
    const templateId = template.get('id') || null
    const req = templateId
      ? this.api.url(`/email_template/${templateId}`, { method: 'PUT' })
      : this.api.url(`/email_template`, { method: 'POST' })
    return req.copy({ body: this.mapToObject(template) }).execute()
  }

  /**
   * FIXME
   * Method created since toJS() was not working at the time. This is a bandaid that should be refactored in future releases
   */
  mapToObject(map) {
    const out = Object.create(null)
    map.forEach((value, key) => {
      if (value instanceof Map) {
        out[key] = this.mapToObject(value)
      } else {
        out[key] = value
      }
    })
    return out
  }
}

export const EmailTemplates = new EmailTemplateClient(Api)
