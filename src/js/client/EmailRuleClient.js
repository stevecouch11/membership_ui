import { Api } from '../client/ApiClient'

export class EmailRuleClient {
  constructor(api) {
    this.api = api
  }

  all() {
    return this.api
      .url('/emails')
      .get()
      .execute()
  }

  create(email) {
    return this.api
      .url('/emails')
      .post(email)
      .execute()
  }

  update(email) {
    return this.api
      .url(`/emails/${email.get('id')}`)
      .put(email)
      .execute()
  }

  delete(email) {
    return this.api
      .url(`/emails/${email.get('id')}`)
      .remove()
      .execute()
  }
}

export const EmailRules = new EmailRuleClient(Api)
