import { Api } from '../client/ApiClient'
import { fromJS } from 'immutable'

export class MeetingClient {
  constructor(api) {
    this.api = api
  }

  isValidMeetingCode(code) {
    let codeInt
    try {
      codeInt = parseInt(code, 10)
    } catch (e) {
      codeInt = 0
    }
    return codeInt >= 1000 && codeInt <= 9999
  }

  create(meeting) {
    const m = fromJS(meeting)
    const startTimeIsoString = m.get('startTime')
      ? m.get('startTime').toISOString()
      : null
    const endTimeIsoString = m.get('endTime')
      ? m.get('endTime').toISOString()
      : null
    const committeeId = m.get('committeeId') || null
    const landingUrl = m.get('landingUrl') || null
    return this.api
      .url(`/meeting`)
      .post({
        name: m.get('name'),
        committee_id: committeeId,
        landing_url: landingUrl,
        start_time: startTimeIsoString,
        end_time: endTimeIsoString
      })
      .execute()
  }

  all() {
    return this.api
      .url(`/meeting/list`)
      .get()
      .execute()
  }

  get(meetingId) {
    return this.api
      .url(`/meetings/${meetingId}`)
      .get()
      .execute()
  }

  getAttendees(meetingId) {
    return this.api
      .url(`/meetings/${meetingId}/attendees`)
      .get()
      .execute()
  }

  addAttendee(meetingId, memberId) {
    return this.api
      .url(`/member/attendee`)
      .post({ meeting_id: meetingId, member_id: memberId })
      .execute()
  }

  updateAttendee(meetingId, memberId, attributes) {
    return this.api
      .url(`/meetings/${meetingId}/attendees/${memberId}`)
      .patch(attributes)
      .execute()
  }

  removeAttendee(meetingId, memberId) {
    return this.api
      .url(`/meetings/${meetingId}/attendees/${memberId}`)
      .remove()
      .execute()
  }

  autogenerateMeetingCode(meetingId) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    return this.api
      .url(`/meeting`)
      .patch({
        meeting_id: meetingId,
        code: 'autogenerate'
      })
      .execute()
  }

  setMeetingCode(meetingId, code) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    let validated
    if (code === '') {
      validated = null
    } else {
      if (!this.isValidMeetingCode(code)) {
        throw new Error(`Invalid meeting code: ${code}`)
      }
      validated = parseInt(code, 10)
    }
    return this.api
      .url(`/meeting`)
      .patch({
        meeting_id: meetingId,
        code: validated
      })
      .execute()
  }

  updateMeeting(meetingId, attributes) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    return this.api
      .url(`/meeting`)
      .patch(attributes.set('meeting_id', meetingId))
      .execute()
  }

  createProxyToken(meetingId) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    return this.api
      .url(`/meetings/${meetingId}/proxy-token`)
      .post()
      .execute()
  }

  getProxyTokenState(meetingId, proxyTokenId) {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    if (!proxyTokenId) {
      throw new Error(`Invalid proxyTokenId: ${proxyTokenId}`)
    }
    return this.api
      .url(
        `/meetings/${meetingId}/proxy-token/${encodeURIComponent(proxyTokenId)}`
      )
      .get()
      .execute()
  }

  acceptOrRejectProxyToken(meetingId, proxyTokenId, action) {
    if (!meetingId) {
      throw new Error(`Invalid meetingId: ${meetingId}`)
    }
    if (!proxyTokenId) {
      throw new Error(`Invalid proxyTokenId: ${proxyTokenId}`)
    }
    if (!action || (action !== 'accept' && action !== 'reject')) {
      throw new Error(
        `Invalid action: ${action}. Valid actions: accept, reject`
      )
    }
    return this.api
      .url(
        `/meetings/${meetingId}/proxy-token/${encodeURIComponent(
          proxyTokenId
        )}/${action}`
      )
      .post()
      .execute()
  }
}

export const Meetings = new MeetingClient(Api)

export default Meetings
