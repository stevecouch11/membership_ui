import { ELECTIONS } from '../constants/actionTypes'
import { fromJS, Map } from 'immutable'

const INITIAL_STATE = fromJS({
  byId: {}
})

function elections(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ELECTIONS.FETCH_ELECTIONS:
      const elections = Map(
        action.payload.map(election => [election.get('id'), election])
      )
      return state.mergeDeepIn(['byId'], elections)
    case ELECTIONS.FETCH_ELECTION:
      const electionId = action.payload.get('id')
      return state.mergeDeepIn(['byId', electionId], action.payload)
    default:
      return state
  }
}

export default elections
