import { List, Map } from 'immutable'
import { fetchDataHandler } from './decorators'
import { FETCH_DATA_SUCCESS } from '../actions/fetchDataActions'

export const MEMBER_STORE = 'member'

export const INITIAL_STATE = Map({
  isAdmin: false,
  user: Map({
    data: null,
    loading: false,
    err: null
  })
})

function member(state = INITIAL_STATE, action) {
  if (action.store !== MEMBER_STORE) {
    return state
  }

  switch (action.type) {
    case FETCH_DATA_SUCCESS:
      return state.set(
        'isAdmin',
        state
          .getIn(['user', 'data', 'roles'], List())
          .some(
            r => r.get('role') === 'admin' && r.get('committee') === 'general'
          )
      )

    default:
      return state
  }
}

export default fetchDataHandler(member, MEMBER_STORE)
