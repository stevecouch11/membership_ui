import { IMPORT_MEMBERS } from '../constants/actionTypes'
import { Map } from 'immutable'

export const INITIAL_STATE = Map()

function memberImport(state = INITIAL_STATE, action) {
  switch (action.type) {
    case IMPORT_MEMBERS.SUBMIT:
      return Map({ state: 'initiated' })
    case IMPORT_MEMBERS.SUCCESS:
      return Map({ state: 'success', results: action.payload })
    case IMPORT_MEMBERS.FAILED:
      return Map({ state: 'failure' })
    default:
      return state
  }
}

export default memberImport
