import { fromJS } from 'immutable'
import * as types from './../constants/actionTypes'
import { showNotification } from '../../util/util'

// todo rework this hierarchy
const INITIAL_STATE = fromJS({
  byId: {},
  specific: {},
  form: {
    edit: {
      inSubmission: false
    }
  }
})

export function templates(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_EMAIL_TEMPLATES_SUCCEEDED:
      return state.set(
        'byId',
        action.payload.toMap().mapEntries(([_, tmpl]) => [tmpl.get('id'), tmpl])
      )
    case types.SAVE_EMAIL_TEMPLATE:
      return state.setIn(['form', 'edit', 'inSubmission'], true)
    case types.SAVE_EMAIL_TEMPLATE_SUCCEEDED:
      showNotification('Saved!', 'Email template saved.')
      const template = action.payload
      return state
        .mergeDeepIn(['byId', template.get('id')], template)
        .setIn(['form', 'edit', 'inSubmission'], false)
    case types.SAVE_EMAIL_TEMPLATE_FAILED:
      return state.setIn(['form', 'edit', 'inSubmission'], false)
    case types.FETCH_EMAIL_TEMPLATE_SUCCEEDED:
      return state.set('template', action.payload)
    default:
      return state
  }
}

export default templates
