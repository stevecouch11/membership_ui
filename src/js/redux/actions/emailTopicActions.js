import * as types from './../constants/actionTypes'
import { EmailTopics } from '../../client/EmailTopicClient'

export function fetchEmailTopics() {
  return async dispatch => {
    const topics = await EmailTopics.all()
    dispatch({
      type: types.FETCH_EMAIL_TOPICS,
      payload: topics
    })
  }
}

export function sendEmailTemplate(topicId, templateId, address) {
  return async dispatch => {
    try {
      EmailTopics.send(topicId, templateId, address)
      dispatch({ type: types.SEND_EMAIL_TEMPLATE_SUCCEEDED })
    } catch (error) {
      logError(JSON.stringify(error))
      dispatch({ type: types.SEND_EMAIL_TEMPLATE_FAILED, payload: error })
    } finally {
      showNotification('Sent!', 'Email template sent!')
    }
  }
}
