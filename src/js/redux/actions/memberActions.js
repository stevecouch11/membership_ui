import { fetchData } from './fetchDataActions'
import { membershipApi } from '../../services/membership'
import { logError } from '../../util/util'
import { MEMBER_STORE as store } from '../reducers/memberReducers'
import { AUTH_STORE } from '../reducers/auth'
import { USE_AUTH } from '../../config'
import { Members } from '../../client/MemberClient'
import {
  IMPORT_MEMBERS,
  MEMBERS_CREATED,
  MEMBERS_FETCHED
} from '../constants/actionTypes'
import { fromJS } from 'immutable'
import { FETCH_DATA_SUCCESS } from '../../redux/actions/fetchDataActions'

export function fetchMember() {
  return (dispatch, getState) => {
    const user = localStorage.user
    if (!USE_AUTH || user !== null) {
      dispatch(
        fetchData({
          apiService: membershipApi,
          route: '/member/details',
          store,
          keyPath: ['user']
        })
      )
    }
  }
}

export function createMember(member) {
  return dispatch => {
    Members.create(member)
      .then(body => {
        const m = body.getIn(['data', 'member'])
        const member = {
          id: m.get('id'),
          email: m.getIn(['info', 'email_address'], ''),
          name:
            m.getIn(['info', 'first_name'], '') +
            ' ' +
            m.getIn(['info', 'last_name'], '')
        }
        dispatch({
          type: MEMBERS_CREATED,
          payload: fromJS({
            member
          })
        })
      })
      .catch(err => {
        logError(err.toString(), err)
      })
  }
}

export function updateMember(attributes) {
  return dispatch => {
    Members.update(attributes)
      .then(body => {
        dispatch({
          store,
          type: FETCH_DATA_SUCCESS,
          keyPath: ['user'],
          data: body.get('data')
        })
      })
      .catch(err => {
        logError(err.toString(), err)
      })
  }
}

/**
 * Fetches all members in the system.
 *
 * @note We should not use this, but the current UI relies un this to show the admin page and my goal
 * is to move everything into redux, so I'm going to refactor that page later.
 */
export function fetchAllMembers() {
  return dispatch => {
    Members.all()
      .then(data => {
        dispatch({
          type: MEMBERS_FETCHED,
          payload: data
        })
      })
      .catch(err => {
        logError(err.toString(), err)
      })
  }
}

export function importRoster(file) {
  return dispatch => {
    dispatch({ type: IMPORT_MEMBERS.SUBMIT })
    Members.importRoster(file)
      .then(data => {
        dispatch({
          type: IMPORT_MEMBERS.SUCCESS,
          payload: data.get('data')
        })
      })
      .catch(err => {
        dispatch({
          type: IMPORT_MEMBERS.FAILED
        })
        logError(err.toString(), err)
      })
  }
}
