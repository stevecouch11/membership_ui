import * as types from './../constants/actionTypes'
import { EmailTemplates } from '../../client/EmailTemplateClient'
import { List } from 'immutable'
import { fromJS } from 'immutable'

export function fetchEmailTemplates() {
  return async dispatch => {
    var templates = await EmailTemplates.all()
    if (templates === undefined) {
      templates = List()
    }
    dispatch({
      type: types.FETCH_EMAIL_TEMPLATES_SUCCEEDED,
      payload: templates
    })
  }
}

export function fetchEmailTemplate(id) {
  return async dispatch => {
    const template = await EmailTemplates.template(id)
    dispatch({
      type: types.FETCH_EMAIL_TEMPLATE_SUCCEEDED,
      payload: fromJS(template)
    })
  }
}

export function saveEmailTemplate(template) {
  return dispatch => {
    EmailTemplates.save(template)
      .then(rsp => {
        dispatch({
          type: types.SAVE_EMAIL_TEMPLATE_SUCCEEDED,
          payload: rsp
        })
      })
      .catch(err => {
        dispatch({
          type: types.SAVE_EMAIL_TEMPLATE_FAILED,
          payload: err
        })
      })
    dispatch({
      type: types.SAVE_EMAIL_TEMPLATE,
      payload: template
    })
  }
}
